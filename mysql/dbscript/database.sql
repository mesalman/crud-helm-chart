/* Create Database and Table */

create database crud_db;
use crud_db; 

GRANT ALL ON crud_db.* to 'php_crud_user'@'%' IDENTIFIED BY 'secret';

CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100),
  `email` varchar(100),
  `mobile` varchar(15),
  PRIMARY KEY  (`id`)
);



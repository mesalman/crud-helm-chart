# Very RAW README

[salman@salmanpc crud-helm]$ mysql -h 127.0.0.1 -u php_crud_user -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MySQL connection id is 4
Server version: 5.7.29 MySQL Community Server (GPL)

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MySQL [(none)]> use php_crud_db;
Database changed
MySQL [php_crud_db]> CREATE TABLE `users` (
    ->   `id` int(11) NOT NULL auto_increment,
    ->   `name` varchar(100),
    ->   `email` varchar(100),
    ->   `mobile` varchar(15),
    ->   PRIMARY KEY  (`id`)
    -> );
Query OK, 0 rows affected (0.086 sec)

MySQL [php_crud_db]> exit
Bye
[salman@salmanpc crud-helm]$ helm list
NAME         	REVISION	UPDATED                 	STATUS  	CHART               	APP VERSION	NAMESPACE
clunky-serval	1       	Fri Apr  3 20:08:12 2020	DEPLOYED	php-crud-chart-0.1.0	1.0        	default  
crud-test-1  	1       	Fri Apr  3 22:12:59 2020	DEPLOYED	php-crud-chart-0.1.0	1.0        	default  
[salman@salmanpc crud-helm]$ helm delete --purge crud-test-1
release "crud-test-1" deleted
[salman@salmanpc crud-helm]$ helm delete --purge clunky-serval
release "clunky-serval" deleted
[salman@salmanpc crud-helm]$ helm install --name myphpapp ./php-crud-chart
NAME:   myphpapp
LAST DEPLOYED: Fri Apr  3 23:08:39 2020
NAMESPACE: default
STATUS: DEPLOYED

RESOURCES:
==> v1/Deployment
NAME      READY  UP-TO-DATE  AVAILABLE  AGE
myphpapp  0/1    1           0          0s

==> v1/Pod(related)
NAME                       READY  STATUS             RESTARTS  AGE
myphpapp-67f68db4cf-lktj6  0/1    ContainerCreating  0         0s

==> v1/Service
NAME      TYPE       CLUSTER-IP   EXTERNAL-IP  PORT(S)  AGE
myphpapp  ClusterIP  10.56.6.156  <none>       80/TCP   0s


[salman@salmanpc crud-helm]$ kubectl get all
NAME                            READY   STATUS    RESTARTS   AGE
pod/myphpapp-67f68db4cf-lktj6   1/1     Running   0          28s
pod/mysql-0                     1/1     Running   0          79m

NAME                 TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)    AGE
service/kubernetes   ClusterIP   10.56.0.1     <none>        443/TCP    4h
service/myphpapp     ClusterIP   10.56.6.156   <none>        80/TCP     28s
service/mysql        ClusterIP   None          <none>        3306/TCP   79m

NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/myphpapp   1/1     1            1           28s

NAME                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/myphpapp-67f68db4cf   1         1         1       28s

NAME                     READY   AGE
statefulset.apps/mysql   1/1     79m
[salman@salmanpc crud-helm]$ cd..
bash: cd..: command not found...
^C
[salman@salmanpc crud-helm]$ cd ..
[salman@salmanpc Projects]$ mv crud-helm crud-helm-chart
[salman@salmanpc Projects]$ cd crud-helm-chart/
[salman@salmanpc crud-helm-chart]$ git init
Initialized empty Git repository in /home/salman/Projects/crud-helm-chart/.git/
[salman@salmanpc crud-helm-chart]$ git remote add origin https://gitlab.com/mesalman/crud-helm-chart.git
[salman@salmanpc crud-helm-chart]$ git add .
[salman@salmanpc crud-helm-chart]$ git commit -m "Initial commit"
[master (root-commit) 6b934a4] Initial commit
 11 files changed, 271 insertions(+)
 create mode 100644 .gitignore
 create mode 100755 create-app-credentials.sh
 create mode 100755 mysql/create-mysql-credentials.sh
 create mode 100644 mysql/dbscript/database.sql
 create mode 100644 mysql/mysql-statefulset-for-wordpress.yaml
 create mode 100644 php-crud-chart/.helmignore
 create mode 100644 php-crud-chart/Chart.yaml
 create mode 100644 php-crud-chart/templates/deployment.yaml
 create mode 100644 php-crud-chart/templates/service.yaml
 create mode 100644 php-crud-chart/values.yaml
 create mode 100644 tiller-rbac.yaml
[salman@salmanpc crud-helm-chart]$ git push -u origin master
Enumerating objects: 17, done.
Counting objects: 100% (17/17), done.
Delta compression using up to 4 threads
Compressing objects: 100% (15/15), done.
Writing objects: 100% (17/17), 3.31 KiB | 1.66 MiB/s, done.
Total 17 (delta 3), reused 0 (delta 0)
To https://gitlab.com/mesalman/crud-helm-chart.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
[salman@salmanpc crud-helm-chart]$ 
[salman@salmanpc crud-helm-chart]$ git status
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
[salman@salmanpc crud-helm-chart]$ ls
create-app-credentials.sh  mysql  php-crud-app.env  php-crud-chart  tiller-rbac.yaml
[salman@salmanpc crud-helm-chart]$ cd php-crud-chart/
[salman@salmanpc php-crud-chart]$ list
bash: list: command not found...
[salman@salmanpc php-crud-chart]$ ls
charts  Chart.yaml  templates  values.yaml
[salman@salmanpc php-crud-chart]$ ls charts
[salman@salmanpc php-crud-chart]$ helm package
Error: need at least one argument, the path to the chart
[salman@salmanpc php-crud-chart]$ helm package .
Successfully packaged chart and saved it to: /home/salman/Projects/crud-helm-chart/php-crud-chart/php-crud-chart-0.1.0.tgz
[salman@salmanpc php-crud-chart]$ ls
charts  Chart.yaml  php-crud-chart-0.1.0.tgz  templates  values.yaml
[salman@salmanpc php-crud-chart]$ rm php-crud-chart-0.1.0.tgz 
[salman@salmanpc php-crud-chart]$ helm package charts/ 
Error: chart metadata (Chart.yaml) missing
[salman@salmanpc php-crud-chart]$ helm package -d charts/ 
Error: need at least one argument, the path to the chart
[salman@salmanpc php-crud-chart]$ helm package . -d charts/ 
Successfully packaged chart and saved it to: charts/php-crud-chart-0.1.0.tgz
[salman@salmanpc php-crud-chart]$ ls
charts  Chart.yaml  templates  values.yaml
[salman@salmanpc php-crud-chart]$ cd charts/
[salman@salmanpc charts]$ ls
php-crud-chart-0.1.0.tgz
[salman@salmanpc charts]$ git status
On branch master
Your branch is up to date with 'origin/master'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	./

nothing added to commit but untracked files present (use "git add" to track)
[salman@salmanpc charts]$ git add .
[salman@salmanpc charts]$ git commit -m 'helm package'
[master f7ea1bf] helm package
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 php-crud-chart/charts/php-crud-chart-0.1.0.tgz
[salman@salmanpc charts]$ git push
Enumerating objects: 7, done.
Counting objects: 100% (7/7), done.
Delta compression using up to 4 threads
Compressing objects: 100% (5/5), done.
Writing objects: 100% (5/5), 1.46 KiB | 1.46 MiB/s, done.
Total 5 (delta 2), reused 0 (delta 0)
To https://gitlab.com/mesalman/crud-helm-chart.git
   6b934a4..f7ea1bf  master -> master
[salman@salmanpc charts]$ helm repo index
Error: This command needs 1 argument: path to a directory
[salman@salmanpc charts]$ helm repo index .
[salman@salmanpc charts]$ ls -la
total 16
drwxrwxr-x 2 salman salman 4096 Apr  3 23:28 .
drwxr-xr-x 4 salman salman 4096 Apr  3 23:21 ..
-rw-r--r-- 1 salman salman  399 Apr  3 23:28 index.yaml
-rw-rw-r-- 1 salman salman 1102 Apr  3 23:22 php-crud-chart-0.1.0.tgz
[salman@salmanpc charts]$ cat index.yaml 
apiVersion: v1
entries:
  php-crud-chart:
  - apiVersion: v1
    appVersion: "1.0"
    created: "2020-04-03T23:28:31.786097766+02:00"
    description: Helm chart with php crud application
    digest: 7fc700609399d551dca33f24d67c1d04c3d382b7e13880cd1d7fda90f926f015
    name: php-crud-chart
    urls:
    - php-crud-chart-0.1.0.tgz
    version: 0.1.0
generated: "2020-04-03T23:28:31.785624354+02:00"
[salman@salmanpc charts]$ git status
On branch master
Your branch is up to date with 'origin/master'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	index.yaml

nothing added to commit but untracked files present (use "git add" to track)
[salman@salmanpc charts]$ git add .
[salman@salmanpc charts]$ git commit -m 'helm index file generated'
[master 04385d0] helm index file generated
 1 file changed, 13 insertions(+)
 create mode 100644 php-crud-chart/charts/index.yaml
[salman@salmanpc charts]$ git push
Enumerating objects: 8, done.
Counting objects: 100% (8/8), done.
Delta compression using up to 4 threads
Compressing objects: 100% (5/5), done.
Writing objects: 100% (5/5), 662 bytes | 662.00 KiB/s, done.
Total 5 (delta 2), reused 0 (delta 0)
To https://gitlab.com/mesalman/crud-helm-chart.git
   f7ea1bf..04385d0  master -> master
[salman@salmanpc charts]$ cd ..
[salman@salmanpc php-crud-chart]$ helm repo add crud-helm-repo  https://gitlab.com/mesalman/crud-helm-chart/-/raw/master/php-crud-chart/charts/
"crud-helm-repo" has been added to your repositories
[salman@salmanpc php-crud-chart]$ helm repo update
Hang tight while we grab the latest from your chart repositories...
...Skip local chart repository
...Successfully got an update from the "crud-helm-repo" chart repository
...Successfully got an update from the "stable" chart repository
...Successfully got an update from the "bitnami" chart repository
Update Complete.
[salman@salmanpc php-crud-chart]$ helm repo list
NAME          	URL                                                                            
stable        	https://kubernetes-charts.storage.googleapis.com                               
local         	http://127.0.0.1:8879/charts                                                   
bitnami       	https://charts.bitnami.com/bitnami                                             
crud-helm-repo	https://gitlab.com/mesalman/crud-helm-chart/-/raw/master/php-crud-chart/charts/
[salman@salmanpc php-crud-chart]$ helm list
NAME    	REVISION	UPDATED                 	STATUS  	CHART               	APP VERSION	NAMESPACE
myphpapp	1       	Fri Apr  3 23:08:39 2020	DEPLOYED	php-crud-chart-0.1.0	1.0        	default  
[salman@salmanpc php-crud-chart]$ helm delete --purge myphpapp
release "myphpapp" deleted
[salman@salmanpc php-crud-chart]$ 


[salman@salmanpc php-crud-chart]$ cd /tmp
[salman@salmanpc tmp]$ helm list
[salman@salmanpc tmp]$ kubectl get all
NAME          READY   STATUS    RESTARTS   AGE
pod/mysql-0   1/1     Running   0          115m

NAME                 TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
service/kubernetes   ClusterIP   10.56.0.1    <none>        443/TCP    4h36m
service/mysql        ClusterIP   None         <none>        3306/TCP   115m

NAME                     READY   AGE
statefulset.apps/mysql   1/1     115m
[salman@salmanpc tmp]$ helm repo list 
NAME          	URL                                                                            
stable        	https://kubernetes-charts.storage.googleapis.com                               
local         	http://127.0.0.1:8879/charts                                                   
bitnami       	https://charts.bitnami.com/bitnami                                             
crud-helm-repo	https://gitlab.com/mesalman/crud-helm-chart/-/raw/master/php-crud-chart/charts/
[salman@salmanpc tmp]$ helm install fabric8/chaos-monkey
Error: failed to download "fabric8/chaos-monkey" (hint: running `helm repo update` may help)
[salman@salmanpc tmp]$ helm install crud-helm-repo/php-crud-chart --name gitlab-crud
NAME:   gitlab-crud
LAST DEPLOYED: Fri Apr  3 23:49:49 2020
NAMESPACE: default
STATUS: DEPLOYED

RESOURCES:
==> v1/Deployment
NAME         READY  UP-TO-DATE  AVAILABLE  AGE
gitlab-crud  0/1    1           0          1s

==> v1/Pod(related)
NAME                          READY  STATUS             RESTARTS  AGE
gitlab-crud-75f8f6cb4c-hp74n  0/1    ContainerCreating  0         1s

==> v1/Service
NAME         TYPE       CLUSTER-IP  EXTERNAL-IP  PORT(S)  AGE
gitlab-crud  ClusterIP  10.56.13.5  <none>       80/TCP   1s


[salman@salmanpc tmp]$ kubectl get all
NAME                               READY   STATUS    RESTARTS   AGE
pod/gitlab-crud-75f8f6cb4c-hp74n   1/1     Running   0          24s
pod/mysql-0                        1/1     Running   0          120m

NAME                  TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)    AGE
service/gitlab-crud   ClusterIP   10.56.13.5   <none>        80/TCP     24s
service/kubernetes    ClusterIP   10.56.0.1    <none>        443/TCP    4h41m
service/mysql         ClusterIP   None         <none>        3306/TCP   120m

NAME                          READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/gitlab-crud   1/1     1            1           24s

NAME                                     DESIRED   CURRENT   READY   AGE
replicaset.apps/gitlab-crud-75f8f6cb4c   1         1         1       24s

NAME                     READY   AGE
statefulset.apps/mysql   1/1     120m
[salman@salmanpc tmp]$ 




